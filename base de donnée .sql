

// -- Table "utilisateurs" pour enregistrer les informations des utilisateurs
CREATE TABLE utilisateurs (
  id INT AUTO_INCREMENT PRIMARY KEY,
  nom VARCHAR(50) NOT NULL,
  prenom VARCHAR(50) NOT NULL,
  email VARCHAR(100) NOT NULL,
  mot_de_passe VARCHAR(255) NOT NULL,
  date_naissance DATE NOT NULL,
  genre VARCHAR(10) NOT NULL,
  adresse VARCHAR(100) NOT NULL,
  telephone VARCHAR(20) NOT NULL
);

// -- Table "education" pour enregistrer les informations sur l'éducation des utilisateurs
CREATE TABLE education (
  id INT AUTO_INCREMENT PRIMARY KEY,
  utilisateur_id INT NOT NULL,
  niveau VARCHAR(50) NOT NULL,
  domaine VARCHAR(50) NOT NULL,
  date_debut DATE NOT NULL,
  date_fin DATE NOT NULL,
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs(id)
// );

// -- Table "competences" pour enregistrer les compétences des utilisateurs
CREATE TABLE competences (
  id INT AUTO_INCREMENT PRIMARY KEY,
  utilisateur_id INT NOT NULL,
  nom VARCHAR(50) NOT NULL,
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateurs(id)
);
