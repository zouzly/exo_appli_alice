<?php
class Competence extends Utilisateur {
    // private $id;
    private $utilisateurId;
    private $nom;
    
    // Constructeur
    public function __construct($utilisateurId, $nom) { 
        parent::__construct("", "", "");
        $this->utilisateurId = $utilisateurId;
        $this->nom = $nom;
    }
    
    // Méthodes getter et setter pour $id
    // public function getId() {
    //     return $this->id;
    // }
    
    // public function setId($id) {
    //     $this->id = $id;
    // }
    
    // Méthodes getter et setter pour $utilisateurId
    public function getUtilisateurId() {
        return $this->utilisateurId;
    }
    
    public function setUtilisateurId($utilisateurId) {
        $this->utilisateurId = $utilisateurId;
    }
    
    // Méthodes getter et setter pour $nom
    public function getNom() {
        return $this->nom;
    }
    
    public function setNom($nom) {
        $this->nom = $nom;
    }
}

?>
