<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>Formulaire</title>
    <link rel="stylesheet" href="design.css"> <!-- Lien vers le fichier CSS -->
</head>

<body>
    <div id="main-container">
        <section id="container-1">
            
        </section>
        <section id="container-2">
            
        </section>
        <section id="container-3">
        </section>
        <section id="container-4">
        </section >
    </section>
    <section id="container-5">
    </section >
            <form id="container-form" method="POST" action="script.php">
                <label for="nom"><strong>NOM :</strong></label><br>
                <input type="text" id="nom" name="nom" required><br><br> 
        
                <!-- Ajoutez ici les autres champs du formulaire avec des balises <label> et <input> correspondantes -->
                <label for="prenom"><strong>PRENOM :</strong></label><br>
                <input type="text" id="prenom" name="prenom" required><br><br>
        
                <label for="email"><strong>EMAIL:</strong></label><br>
                <input type="email" id="email" name="email" required><br><br>
        
                <label for="password"><strong>PASSWORD :</strong></label><br>
                <input type="password" id="password" name="password" required><br><br>
        
                <label for="date_de_naissance"><strong>DATE DE NAISSANCE:</strong></label><br>
                <input type="date" id="date_de_naissance" name="date_de_naissance" required><br><br>
        
                <label for="genre"><strong>GENRE :</strong></label><br>
                <select id="genre" name="genre" required><br><br>
                    <option value="homme">Homme</option><br><br>
                    <option value="femme">Femme</option><br><br>
                    <option value="autre">Autre</option><br><br>
                </select><br>
        
                <label for="adresse"><strong>ADRESSE :</strong></label><br>
                <input type="text" id="adresse" name="adresse" required><br><br>
        
                <label for="telephone"><strong>TEL :</strong></label><br>
                <input type="tel" id="telephone" name="telephone" required><br><br>
                <div id="beaute-eternelle">
                    <h2>A un<input type="submit" value= "Clic" id="join"> de la beauté éternelle</h2>
                </div>
                
                
            </form>
        </section>
        
        <!-- <img src="eden/infini.jpg" alt=""> -->
    </div>
    <h1><strong>EDEN CLINIC</strong></h1>
    
    <!-- 
    
    <input type="submit" value="ENVOYEZ"><br><br>

    <form method="POST" action="<?php echo $_SERVER["PHP_SELF"]; ?>
        <label for="nom"><strong>NOM :</strong></label><br>
        <input type="text" id="nom" name="nom" required><br><br> -->

        <!-- Ajoutez ici les autres champs du formulaire avec des balises <label> et <input> correspondantes -->
        <!-- <label for="prenom"><strong>PRENOM :</strong></label><br>
        <input type="text" id="prenom" name="prenom" required><br><br>

        <label for="email"><strong>EMAIL:</strong></label><br>
        <input type="email" id="email" name="email" required><br><br>

        <label for="password"><strong>PASSWORD :</strong></label><br>
        <input type="password" id="password" name="password" required><br><br>

        <label for="date_de_naissance"><strong>DATE DE NAISSANCE:</strong></label><br>
        <input type="date" id="date_de_naissance" name="date_de_naissance" required><br><br>

        <label for="genre"><strong>GENRE :</strong></label><br>
        <select id="genre" name="genre" required><br><br>
            <option value="homme">Homme</option><br><br>
            <option value="femme">Femme</option><br><br>
            <option value="autre">Autre</option><br><br>
        </select><br>

        <label for="adresse"><strong>ADRESSE :</strong></label><br>
        <input type="text" id="adresse" name="adresse" required><br><br>

        <label for="telephone"><strong>TEL :</strong></label><br>
        <input type="tel" id="telephone" name="telephone" required><br><br>

        
    </form> -->
    <script src="photos.js"></script>
</body>

</html>
