<?php

class Utilisateur  {
    private $id;
    private $nom;
    private $prenom;
    private $email;
    
    // Constructeur
    public function __construct($nom, $prenom, $email) {
     
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->email = $email;
    }
    
    // Méthodes getter et setter pour chaque propriété
    public function getId() {
        return $this->id;
    }
    
    public function getNom() {
        return $this->nom;
    }
    
    public function setNom($nom) {
        $this->nom = $nom;
    }
    
    public function getPrenom() {
        return $this->prenom;
    }
    
    public function setPrenom($prenom) {
        $this->prenom = $prenom;
    }
    
    public function getEmail() {
        return $this->email;
    }
    
    public function setEmail($email) {
        $this->email = $email;
    }
}

// class Education {
//     private $id;
//     private $utilisateurId;
//     private $niveau;
//     private $domaine;
    
//     // Constructeur
//     public function __construct($utilisateurId, $niveau, $domaine) {
//         $this->utilisateurId = $utilisateurId;
//         $this->niveau = $niveau;
//         $this->domaine = $domaine;
//     }
    
//     // Méthodes getter et setter pour chaque propriété
//     // ...
// }

// class Competence {
//     private $id;
//     private $utilisateurId;
//     private $nom;
    
//     // Constructeur
//     public function __construct($utilisateurId, $nom) {
//         $this->utilisateurId = $utilisateurId;
//         $this->nom = $nom;
//     }
    
//     // Méthodes getter et setter pour chaque propriété
//     // ...
// }
?>