
<?php
class Education extends Utilisateur {
    // private $id;
    private $utilisateurId;
    private $niveau;
    private $domaine;
    private $dateDebut;
    private $dateFin;
    
    // Constructeur
    public function __construct($utilisateurId, $niveau, $domaine, $dateDebut, $dateFin) {
        parent::__construct("", "", "");
        $this->utilisateurId = $utilisateurId;
        $this->niveau = $niveau;
        $this->domaine = $domaine;
        $this->dateDebut = $dateDebut;
        $this->dateFin = $dateFin;
    }
    
    // Méthodes getter et setter pour $id
    // public function getId() {
    //     return $this->id;
    // }
    
    // public function setId($id) {
    //     $this->id = $id;
    // }
    
    // Méthodes getter et setter pour $utilisateurId
    public function getUtilisateurId() {
        return $this->utilisateurId;
    }
    
    public function setUtilisateurId($utilisateurId) {
        $this->utilisateurId = $utilisateurId;
    }
    
    // Méthodes getter et setter pour $niveau
    public function getNiveau() {
        return $this->niveau;
    }
    
    public function setNiveau($niveau) {
        $this->niveau = $niveau;
    }
    
    // Méthodes getter et setter pour $domaine
    public function getDomaine() {
        return $this->domaine;
    }
    
    public function setDomaine($domaine) {
        $this->domaine = $domaine;
    }
    
    // Méthodes getter et setter pour $dateDebut
    public function getDateDebut() {
        return $this->dateDebut;
    }
    
    public function setDateDebut($dateDebut) {
        $this->dateDebut = $dateDebut;
    }
    
    // Méthodes getter et setter pour $dateFin
    public function getDateFin() {
        return $this->dateFin;
    }
    
    public function setDateFin($dateFin) {
        $this->dateFin = $dateFin;
    }
}

?>